import { Component, OnInit } from '@angular/core';
import { Title } from "@angular/platform-browser";

@Component({
  selector: 'app-about-us2',
  templateUrl: './about-us2.component.html',
  styleUrls: ['./about-us2.component.css']
})
export class AboutUs2Component implements OnInit {

  isCollapsed: boolean = true;

  constructor(private titleService: Title) {
    this.titleService.setTitle('Omaro');
  }


  ngOnInit() {
  }

  navbarOpen = false;

  toggleNavbar() {
    this.navbarOpen = !this.navbarOpen;
  }


  toggleCollapse() {
    this.isCollapsed = !this.isCollapsed;
  }


}
