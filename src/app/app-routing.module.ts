import { AboutUs2Component } from './about-us2/about-us2.component';
import { AboutUsComponent } from './about-us/about-us.component';
import { OpenWardrobeComponent } from './open-wardrobe/open-wardrobe.component';
import { CarouselComponent } from './carousel/carousel.component';
import { OutfitComponent } from './outfit/outfit.component';
import { HomepageComponent } from './homepage/homepage.component';
import { UploadComponent } from './upload/upload.component';
import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';



const routes: Routes = [
  { path: 'aboutus', component: AboutUs2Component},
  { path: 'open', component: OpenWardrobeComponent},
  { path: 'carousel', component: CarouselComponent },
  { path: 'outfit', component: OutfitComponent },
  { path: 'upload', component: UploadComponent },
  { path: 'homepage', component: HomepageComponent},
  { path: '', redirectTo: '/homepage', pathMatch: 'full' }
];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
