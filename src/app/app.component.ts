import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { OpenWardrobeComponent } from './open-wardrobe/open-wardrobe.component';
import { AppRoutingModule } from './app-routing.module';
import { BrowserModule } from '@angular/platform-browser';
import { OutfitComponent } from './outfit/outfit.component';
import { HomepageComponent } from './homepage/homepage.component';
import { UploadComponent } from './upload/upload.component';
import { Component, OnInit, ViewEncapsulation, NgModule,ViewChild } from '@angular/core';
import { CarouselComponent } from "./carousel/carousel.component";
import {HttpClient} from '@angular/common/http';
import {DescriptionModel} from './description-model';
import {ImageModel} from './image-model';
import {Tag} from './tag';
import { Title } from "@angular/platform-browser";

import {
  trigger,
  state,
  style,
  animate,
  transition,
  group
}from '@angular/animations';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],

})


export class AppComponent {
  constructor(private titleService: Title) {
    this.titleService.setTitle('Omaro');
  }


}
