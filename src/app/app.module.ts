import {ModalComponent} from './_modal/modal.component';

import {ModalModule} from './_modal';

import { OutfitComponent } from './outfit/outfit.component';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {UploadComponent} from './upload/upload.component';
import { HomepageComponent } from './homepage/homepage.component';
import { FormsModule } from '@angular/forms';
import { CarouselComponent } from './carousel/carousel.component';
import {HttpClientModule} from '@angular/common/http';
import { OpenWardrobeComponent } from './open-wardrobe/open-wardrobe.component';
import { GridsterModule } from 'angular2gridster';
import { AboutUsComponent } from './about-us/about-us.component';
import { AboutUs2Component } from './about-us2/about-us2.component';


@NgModule({
  declarations: [
    AppComponent,
    UploadComponent,
    HomepageComponent,
    OutfitComponent,
    CarouselComponent,
    OpenWardrobeComponent,
    AboutUsComponent,
    AboutUs2Component

  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    GridsterModule.forRoot(), // .forRoot() is required since version v4+
    ModalModule,
    HttpClientModule


  ],
  providers: [],
  bootstrap: [AppComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppModule { }
