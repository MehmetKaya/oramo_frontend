import {Component, OnInit, ViewChild} from '@angular/core';
import {Tag} from '../tag';
import {ImageModel} from '../image-model';
import {DescriptionModel} from '../description-model';
import {HttpClient} from '@angular/common/http';
import { Title } from "@angular/platform-browser";

@Component({
  selector: 'app-carousel',
  templateUrl: './carousel.component.html',
  styleUrls: ['./carousel.component.css']
})
export class CarouselComponent implements OnInit {

  constructor(private httpClient: HttpClient,private titleService: Title) {
    this.titleService.setTitle('Omaro');
  }

  images: Array<ImageModel>;

  ngOnInit() {
      this.findImagesPerDescription('Jacket');
  }


  findImagesPerDescription(tagContent) {

    const tagModel: Tag = {
      tag: tagContent
    };

    const response = this.httpClient.post<ImageModel []>('http://localhost:8081/check//findImagesByDescription', tagModel);
    response.subscribe(res => {
      this.images = res;

      // Loop for assigning base64 Data to images
      for (const image of this.images) {
        image.pic = 'data:image/jpeg;base64,' + image.pic;
      }
    });
  }
}
