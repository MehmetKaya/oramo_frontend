export interface DescriptionModel {
  id: number;
  description: string;
  imageId: number;
}
