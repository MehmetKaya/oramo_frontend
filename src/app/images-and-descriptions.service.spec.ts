import { TestBed } from '@angular/core/testing';

import { ImagesAndDescriptionsService } from './images-and-descriptions.service';

describe('ImagesAndDescriptionsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ImagesAndDescriptionsService = TestBed.get(ImagesAndDescriptionsService);
    expect(service).toBeTruthy();
  });
});
