import { Injectable } from '@angular/core';
import { DescriptionModel} from './description-model';
import { ImageModel} from './image-model';

@Injectable({
  providedIn: 'root'
})
export class ImagesAndDescriptionsService {

  // This section is for uploading Images and Tags
  title = 'ImageUploaderFrontEnd';
  public selectedFile;
  public event1;
  imgURL: any;
  receivedImageData: any;
  base64Data: any;
  convertedImage: any;
  descriptions: Array<DescriptionModel>;
  imageId: any;
  tagContent: any;
  tagsSaved = true;

  // This section is for showing Images
  images: Array<ImageModel>;

  constructor() { }

  getDescriptions(): DescriptionModel[] {
    return this.descriptions;
  }

  getImages(): ImageModel[] {
    return  this.images;
  }

  getConvertedImage(): any {
    return this.convertedImage;
  }
}


