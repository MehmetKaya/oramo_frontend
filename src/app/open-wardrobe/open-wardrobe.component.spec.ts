import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OpenWardrobeComponent } from './open-wardrobe.component';

describe('OpenWardrobeComponent', () => {
  let component: OpenWardrobeComponent;
  let fixture: ComponentFixture<OpenWardrobeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OpenWardrobeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OpenWardrobeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
