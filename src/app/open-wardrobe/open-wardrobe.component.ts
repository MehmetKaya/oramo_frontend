import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BrowserModule } from '@angular/platform-browser';
import { UploadComponent } from './../upload/upload.component';
import { OutfitComponent } from './../outfit/outfit.component';
import { HomepageComponent } from './../homepage/homepage.component';
import { Component, OnInit } from '@angular/core';
import {
  trigger,
  state,
  style,
  animate,
  transition,
  group,
}from '@angular/animations';



@Component({
  selector: 'app-open-wardrobe',
  templateUrl: './open-wardrobe.component.html',
  styleUrls: ['./open-wardrobe.component.css'],

})
export class OpenWardrobeComponent implements OnInit {

  isOpen;
  toggle() {
    this.isOpen=!this.isOpen;

    console.log(this.isOpen);

  }
  constructor() { }

  ngOnInit() {
  }

}
