
import { OutfitComponent } from './outfit.component';
import { HomepageComponent } from './../homepage/homepage.component';
import { UploadComponent } from './../upload/upload.component';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';




describe('OutfitComponent', () => {
  let component: OutfitComponent;
  let fixture: ComponentFixture<OutfitComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OutfitComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OutfitComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
