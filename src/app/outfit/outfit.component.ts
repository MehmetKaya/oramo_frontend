import { ModalComponent } from './../_modal/modal.component';

import { HomepageComponent } from './../homepage/homepage.component';
import { UploadComponent } from './../upload/upload.component';
import { Component, OnInit } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {DescriptionModel} from '../description-model';
import {ImageModel} from '../image-model';
import {Tag} from '../tag';
import { ModalService } from '../_modal';
import { Title } from "@angular/platform-browser";


@Component({
  selector: 'app-outfit',
  templateUrl: './outfit.component.html',
  styleUrls: ['./outfit.component.css']
})

export class OutfitComponent implements OnInit {

  constructor(private httpClient: HttpClient, private modalService: ModalService,private titleService: Title) {
    this.titleService.setTitle('Omaro');
  }

  // This section is for uploading Images and Tags
  title = 'ImageUploaderFrontEnd';
  public selectedFile;
  public event1;
  imgURL: any;
  receivedImageData: any;
  base64Data: any;
  convertedImage: any;
  descriptions: Array<DescriptionModel>;
  imageId: any;
  tagContent: any;
  tagsSaved = true;
  tags: any[];
  i: number;
  j: number;

  // This section is for showing Images
  images: Array<ImageModel>;


  ngOnInit() {
    const tags: string[] = ['Hat', 'Jacket', 'Sweater', 'Dress shirt', 'Pants', 'Shoe'];
    this.tags = tags;
    this.i = 0;
    this.j = 0;
  }


  findOutfit() {

    const response = this.httpClient.post<ImageModel []>('http://localhost:8081/check/findOutfit', null);
    response.subscribe(res => {
      this.images = res;

      // Loop for assigning base64 Data to images
      for (const image of this.images) {
        image.pic = 'data:image/jpeg;base64,' + image.pic;
      }
    });
  }

  isCollapsed: boolean = true;

  navbarOpen = false;

  toggleNavbar() {
    this.navbarOpen = !this.navbarOpen;
  }


  toggleCollapse() {
    this.isCollapsed = !this.isCollapsed;
  }


  getTag() {
    if (this.i === 7) {
      this.i = 0;
    }
    this.i++;
    return this.tags[(this.i - 1)];

  }

  getTag2() {
    if (this.j === 7) {
      this.j = 0;
    }
    this.j++;
    return this.tags[(this.j - 1)];
  }
}
