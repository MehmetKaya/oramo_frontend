import { HomepageComponent } from './../homepage/homepage.component';
import { OutfitComponent } from './../outfit/outfit.component';
import { Component, OnInit } from '@angular/core';
import {DescriptionModel} from '../description-model';
import {ImageModel} from '../image-model';
import {Tag} from '../tag';
import {HttpClient} from '@angular/common/http';
import { Title } from "@angular/platform-browser";

@Component({
  selector: 'app-upload',
  templateUrl: './upload.component.html',
  styleUrls: ['./upload.component.css']
})
export class UploadComponent implements OnInit {

  constructor(private httpClient: HttpClient,private titleService: Title) {
    this.titleService.setTitle('Omaro');
  }

  // This section is for uploading Images and Tags
  title = 'ImageUploaderFrontEnd';
  public selectedFile;
  public event1;
  imgURL: any;
  receivedImageData: any;
  base64Data: any;
  convertedImage: any;
  descriptions: Array<DescriptionModel>;
  imageId: any;
  tagContent: any;
  tagsSaved = true;

  // This section is for showing Images
  images: Array<ImageModel>;

  ngOnInit() {
  }
  public onFileChanged(event) {
    console.log(event);
    this.selectedFile = event.target.files[0];

    // Below part is used to display the selected image
    const reader = new FileReader();
    reader.readAsDataURL(event.target.files[0]);
    reader.onload = (event2) => {
      this.imgURL = reader.result;
    };

  }


  // This part is for uploading
  onUpload() {

    const uploadData = new FormData();
    uploadData.append('myFile', this.selectedFile, this.selectedFile.name);

    // Picture is saved in database and sent back as base64 data
    const response = this.httpClient.post('http://localhost:8081/check/upload', uploadData)
      .subscribe(
        res => {
          console.log(res);
          this.receivedImageData = res;
          this.base64Data = this.receivedImageData.pic;
          this.convertedImage = 'data:image/jpeg;base64,' + this.base64Data;
          this.imageId = this.receivedImageData.id;
          console.log(this.imageId);
          // Picture is sent to backEnd and vision-method is used for creating tags
          // Tags are sent back to frontEnd
          const response2 = this.httpClient.post<DescriptionModel[]>('http://localhost:8081/vision', uploadData);
          response2.subscribe(res2 => {
            this.descriptions = res2;
            for (const description of this.descriptions) {
              description.imageId = this.receivedImageData.id;
            }
          });
         },

        err => console.log('Error Occured during saving: ' + err)
      );

  }
  saveTags() {
    // initialize param for the http-Method
    let descriptions = new Array<DescriptionModel>();
    descriptions = this.descriptions;

    for (const description of this.descriptions) {
       console.log(description);
     }
    // gives descriptions to the backEnd to save it
    const response = this.httpClient.post('http://localhost:8081/check/saveDescriptions', descriptions);
    response.subscribe(res => {
      console.log(res);
      this.tagsSaved = !res;
    });
  }

}
